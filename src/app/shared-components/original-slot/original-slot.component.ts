import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'original-slot',
  templateUrl: './original-slot.component.html',
  styleUrls: ['./original-slot.component.less']
})
export class OriginalSlotComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
