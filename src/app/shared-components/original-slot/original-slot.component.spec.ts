import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OriginalSlotComponent } from './original-slot.component';

describe('OriginalSlotComponent', () => {
  let component: OriginalSlotComponent;
  let fixture: ComponentFixture<OriginalSlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OriginalSlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OriginalSlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
