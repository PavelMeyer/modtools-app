import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostBinding, Input, OnChanges, OnInit} from '@angular/core';
import {Slots} from '../../shared/models';


@Component({
  selector: 'slot-dropdown',
  templateUrl: './slot-dropdown.component.html',
  styleUrls: ['./slot-dropdown.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlotDropdownComponent implements OnInit, OnChanges {

  @Input() slot: Slots = null;
  @Input() parentElement: ElementRef;
  @HostBinding('style.top.px') topOffset: number;

  constructor(private cd: ChangeDetectorRef) {

  }

  ngOnInit(): void {
    this.calculatePosition(this.parentElement?.nativeElement as HTMLElement);
  }

  ngOnChanges(changes: SlotDropdownChanges): void {
    this.calculatePosition(changes.parentElement.nativeElement as HTMLElement)
  }

  private calculatePosition(parentElement: HTMLElement) {
    if (parentElement) {
      const {height} = parentElement?.getBoundingClientRect();
      if (height) {
        this.topOffset = height;
        this.cd.markForCheck();
      }
    }
  }

}

interface SlotDropdownChanges {
  parentElement?: ElementRef;
  slot?: Slots
}
