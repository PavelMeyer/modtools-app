import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SlotDropdownComponent} from './slot-dropdown.component';
import {SharedModule} from '../../shared/shared.module';

describe('SlotDropdownComponent', () => {
  let component: SlotDropdownComponent;
  let fixture: ComponentFixture<SlotDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [SlotDropdownComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlotDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
