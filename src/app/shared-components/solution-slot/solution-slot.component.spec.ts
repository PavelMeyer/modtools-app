import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolutionSlotComponent } from './solution-slot.component';

describe('SolutionSlotComponent', () => {
  let component: SolutionSlotComponent;
  let fixture: ComponentFixture<SolutionSlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolutionSlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolutionSlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
