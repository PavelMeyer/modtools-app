import {Component, ElementRef, HostBinding, HostListener, Input, OnInit} from '@angular/core';
import {Rule, Slots} from '../../shared/models';
import {AppService} from '../../shared/services/app.service';

@Component({
  selector: 'solution-slot',
  templateUrl: './solution-slot.component.html',
  styleUrls: ['./solution-slot.component.less']
})
export class SolutionSlotComponent implements OnInit {

  @HostBinding('style.background-color') bgColor: string;
  @Input() slot: Slots;
  isDropdownVisible = false;
  private solutionToken: Rule = null;

  constructor(public elementRef: ElementRef, private appService: AppService) {
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.isDropdownVisible = true;
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.isDropdownVisible = false;
  }

  ngOnInit(): void {
    this.calculateBgColor();
  }

  private calculateBgColor() {
    this.solutionToken = this.appService.getSolutionToken(this.slot);
    if (this.solutionToken) {
      const maxRisk = Math.max(...Array.from(Object.values(this.solutionToken?.topics)));
      this.bgColor = this.appService.getRiskColor(maxRisk);
    }
  }

}
