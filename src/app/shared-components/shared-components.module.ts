import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';

import {AppGridComponent} from './app-grid/app-grid.component';
import {BreadcrumbsComponent} from './breadcrumbs/breadcrumbs.component';
import {LoadingIndicatorComponent} from './loading-indicator/loading-indicator.component';
import {MoreButtonComponent} from './more-button/more-button.component';
import {OptionButtonGroupComponent} from './option-button-group/option-button-group.component';
import {SolutionSlotComponent} from './solution-slot/solution-slot.component';
import {OriginalSlotComponent} from './original-slot/original-slot.component';
import {ServerPickerComponent} from './server-picker/server-picker.component';
import {ContentTypePickerComponent} from './cotent-type-picker';
import {SlotDropdownComponent} from './slot-dropdown';
import {SharedModule} from '../shared/shared.module';
import {DiagnoseRefreshComponent} from './diagnose-refresh';


@NgModule({
  declarations: [
    AppGridComponent,
    BreadcrumbsComponent,
    LoadingIndicatorComponent,
    MoreButtonComponent,
    OptionButtonGroupComponent,
    SolutionSlotComponent,
    OriginalSlotComponent,
    ServerPickerComponent,
    ContentTypePickerComponent,
    SlotDropdownComponent,
    DiagnoseRefreshComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
  ],
  exports: [
    AppGridComponent,
    BreadcrumbsComponent,
    LoadingIndicatorComponent,
    MoreButtonComponent,
    OptionButtonGroupComponent,
    SolutionSlotComponent,
    OriginalSlotComponent,
    ServerPickerComponent,
    ContentTypePickerComponent,
    SlotDropdownComponent,
    DiagnoseRefreshComponent
  ]
})
export class SharedComponentsModule {
}
