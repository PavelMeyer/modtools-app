import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {DiagnoseService} from '../../shared/services/diagnose.service';

@Component({
  selector: 'diagnose-refresh',
  templateUrl: './diagnose-refresh.component.html',
  styleUrls: ['./diagnose-refresh.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DiagnoseRefreshComponent implements OnInit {

  constructor(private diagnoseService: DiagnoseService) {
  }

  ngOnInit(): void {
  }

  public onDiagnoseRefresh() {
    this.diagnoseService.refresh();
  }
}
