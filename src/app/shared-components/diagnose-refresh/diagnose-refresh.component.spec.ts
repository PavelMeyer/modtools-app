import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnoseRefreshComponent } from './diagnose-refresh.component';
import {HttpClientModule} from '@angular/common/http';

describe('DiagnoseRefreshComponent', () => {
  let component: DiagnoseRefreshComponent;
  let fixture: ComponentFixture<DiagnoseRefreshComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [ DiagnoseRefreshComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnoseRefreshComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
