import { Component, OnInit } from '@angular/core';
import {AppService} from '../../shared/services/app.service';

@Component({
  selector: 'content-type-picker',
  templateUrl: './content-type-picker.component.html',
  styleUrls: ['./content-type-picker.component.less']
})
export class ContentTypePickerComponent implements OnInit {

  constructor(public appService: AppService) { }

  ngOnInit(): void {
  }

  public onTypeChanged(event: Event) {
    // @ts-ignore
    this.appService.setContentType(event.currentTarget.value);
  }

}
