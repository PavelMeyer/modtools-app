import {Component, OnInit} from '@angular/core';
import {AppService} from '../../shared/services/app.service';

@Component({
  selector: 'server-picker',
  templateUrl: './server-picker.component.html',
  styleUrls: ['./server-picker.component.less']
})
export class ServerPickerComponent implements OnInit {

  constructor(public appService: AppService) {
  }

  ngOnInit(): void {
  }

  onServerChanged(event: Event) {

    // todo fix types
    // @ts-ignore
    this.appService.setClient(event.currentTarget.value);
  }

}
