import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerPickerComponent } from './server-picker.component';

describe('ServerPickerComponent', () => {
  let component: ServerPickerComponent;
  let fixture: ComponentFixture<ServerPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
