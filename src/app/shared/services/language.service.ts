import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {LanguageCode} from '../models';
import {UserService} from '../../shared-components/user.service';

@Injectable({providedIn: 'root'})
export class LanguageService {

  constructor(private userService: UserService) {
  }

  _languageCode: BehaviorSubject<LanguageCode> = new BehaviorSubject('en');
  public languageCode$: Observable<LanguageCode> = this._languageCode.asObservable();

  public setLanguageCode(languageCode: LanguageCode) {
    this._languageCode.next(languageCode);

    // TODO #codereview This smells bad.
    // Oughtn't I be able to write this as this.UserService.preferences.language = newLanguage?
    const prefs = this.userService.preferences;
    prefs.language = languageCode;
    this.userService.preferences = prefs;
  }

  get languageCode() {
    return this._languageCode.value;
  }
}
