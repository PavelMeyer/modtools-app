import {Injectable} from '@angular/core';
import {Rule, Slots} from '../models';
import {API_CLIENTS, ClientConfig, CONTENT_TYPES, ContentType} from '../../../constants';


@Injectable({
  providedIn: 'root'
})
export class AppService {

  private _clientConfig: ClientConfig = API_CLIENTS[0];

  get clientConfig() {
    return this._clientConfig;
  }

  private _contentType: ContentType = CONTENT_TYPES[0];

  get contentType(): ContentType {
    return this._contentType
  }

  get configList(): ClientConfig[] {
    return API_CLIENTS;
  }

  get contentTypeList() {
    return CONTENT_TYPES;
  }

  setContentType(contentTypeName: string) {
    this._contentType = CONTENT_TYPES.find(type => type.name === contentTypeName) || this.contentType;
  }

  setClient(configName: string) {
    this._clientConfig = API_CLIENTS.find(config => config.name === configName) || this.clientConfig;
  }

  getRiskColor(riskLevel: number): string {
    if (!window || !document) {
      return null
    }
    return getComputedStyle(document?.documentElement)?.getPropertyValue(`--risk-${riskLevel}`) || null;
  }

  getSolutionToken(slot: Slots): Rule {
    return slot?.tokens?.find(token => token.text === slot.solution) || null;
  }


}
