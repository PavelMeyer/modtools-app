import {Injectable} from '@angular/core';
import {LanguageService} from './language.service';
import {LanguageCode, TextClassifiedOutput, TextInput} from '../models';
import {BehaviorSubject, Observable} from 'rxjs';
import {ApiService} from './api.service';
import {take, tap} from 'rxjs/operators';
import {AppService} from './app.service';

@Injectable({providedIn: 'root'})
export class DiagnoseService {

  private languageCode: LanguageCode;
  private _classifyValue: BehaviorSubject<TextClassifiedOutput> = new BehaviorSubject<TextClassifiedOutput>(null);

  public classifyValue$: Observable<TextClassifiedOutput> = this._classifyValue.asObservable();

  constructor(
    private languageService: LanguageService,
    private apiService: ApiService,
    private appService: AppService
  ) {
    this.languageService.languageCode$.subscribe(code => this.languageCode = code);

  }

  /**
   * plainTextToTextInput
   * @param input - plain text to be classified
   * @return TextInput object for classifyText API
   */
  public plainTextToTextInput(input: string): TextInput {
    return {
      clientId: this.appService.clientConfig.clientId,
      text: input,
      language: this.languageCode || this.languageService.languageCode,
      contentType: this.appService.contentType.type
    };
  }


  /**
   * diagnose text
   *
   * wraps plains text into TextInput object and call the classifyText api with it
   *
   * @param text - plain text to be classified
   * @return Observable<TextClassifiedOutput> - text classification result
   */

  public diagnoseText(text: string): Observable<TextClassifiedOutput> {
    return this.apiService.classifyText(this.plainTextToTextInput(text))
    .pipe(tap(result => this._classifyValue.next(result)))
  }

  public getTopicGuidelines(classifyResult: TextClassifiedOutput): { passesPrivateChat: boolean, passesGlobalChat: boolean } {
    const passesPrivateChat = !Object.values(classifyResult.topics).find(topic => topic >= 6);
    const passesGlobalChat = passesPrivateChat && (!classifyResult.topics[5] || (classifyResult.topics[5] < 5));

    return {
      passesPrivateChat, passesGlobalChat
    }
  }

  public refresh() {
    if (this._classifyValue?.value?.text) {
      this.diagnoseText(this._classifyValue.value.text)
      .pipe(take(1))
      .subscribe()
    }
  }

}
