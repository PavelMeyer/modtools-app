import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceRisk'
})
export class ReplaceRiskPipe implements PipeTransform {

  transform(template: string, risk: number): unknown {
    return template.replace('${risk}', risk.toString(10));
  }

}
