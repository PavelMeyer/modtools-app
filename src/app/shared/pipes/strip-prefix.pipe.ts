import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'stripPrefix'
})
export class StripPrefixPipe implements PipeTransform {

  transform(prefixedValue: string | number): string {
    return prefixedValue.toString(10).replace(/(.)+_/, '');
  }

}
