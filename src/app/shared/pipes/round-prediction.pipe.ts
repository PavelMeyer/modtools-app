import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'roundPrediction'
})
export class RoundPredictionPipe implements PipeTransform {
  /**
   * Accept a text classify predictions and returns the prediction value rounded to integer in percents
   * @param prediction - prediction value
   * @returns prediction value in percents, rounded to integer
   */

  transform(prediction: any): number {
    return Math.floor(prediction * 100);
  }

}
