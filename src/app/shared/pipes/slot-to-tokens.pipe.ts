import {Pipe, PipeTransform} from '@angular/core';
import {Rule, Slots} from '../models';

@Pipe({
  name: 'slotToTokens'
})
export class SlotToTokensPipe implements PipeTransform {


  /**
   * Accept a slot object and returns an array of its tokens with the solution token at the start
   * @param slot - the slot object
   * @returns a sorted array of tokens
   */
  transform(slot: Slots): Rule[] {
    if (!slot?.tokens || !Array.isArray(slot.tokens)) {
      return [];
    }

    return slot.tokens.sort((token1, token2) => {
      if (token2.text === slot.solution) {
        return 1
      } else if (token1.text === slot.solution) {
        return -1
      }
      return 0;
    });
  }

}
