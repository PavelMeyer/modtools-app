import { Pipe, PipeTransform } from '@angular/core';
import {TextClassifiedOutput} from '../models';
import {Topics} from '../../../constants';

@Pipe({
  name: 'slotToTopics'
})
export class SlotToTopicsPipe implements PipeTransform {

  /**
   * Accept a result of text classification and returns an array of its unique topics
   * @param textOutput - the slot object
   * @returns a sorted array of tokens
   */
  transform(textOutput: TextClassifiedOutput): Topics[] {
    return null;
  }

}
