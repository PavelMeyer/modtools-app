import {Pipe, PipeTransform} from '@angular/core';
import {Slots} from '../models';

@Pipe({
  name: 'mergeExtended'
})
export class MergeExtendedPipe implements PipeTransform {

  transform(extended: Slots[], ...args: unknown[]): Slots[] {
    if (!extended) {
      return
    }
    const mergedSlots = [];
    let skip = false;

    extended.forEach((slot, index) => {
      if (skip) {
        skip = false;
        return
      }

      if (slot.solution === extended[index + 1]?.solution) {
        skip = true;
        return mergedSlots.push(this.mergeSlots(slot, extended[index + 1]));
      }

      return mergedSlots.push(slot)
    });
    return mergedSlots;

  }

  private mergeSlots(slot: Slots, slotToMerge: Slots): Slots {
    return {
      original: slot.original + ' ' + slotToMerge.original,
      text: slot.text,
      solution: slot.solution,
      tokens: [...slot.tokens, ...slotToMerge.tokens]
    };
  }
}
