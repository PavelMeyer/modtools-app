import {LanguageCode} from './languageCode';

export interface Rule {
  text: string;
  topics: { [key: string]: number };
  language: LanguageCode;
  altSpellings: string[];
  altSenses: string[];
  leetMappings: string[];
  flags: string[];
  taskIds: string[]
  dateCreated: number;
  dateUpdated: number;
  clientId: number
}
