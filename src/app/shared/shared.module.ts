import {NgModule} from '@angular/core';
import {MergeExtendedPipe, ReplaceRiskPipe, RoundPredictionPipe, SlotToTokensPipe, SlotToTopicsPipe, StripPrefixPipe} from './pipes';

@NgModule({
  imports: [],
  declarations: [
    MergeExtendedPipe,
    SlotToTokensPipe,
    SlotToTopicsPipe,
    ReplaceRiskPipe,
    StripPrefixPipe,
    RoundPredictionPipe
  ],
  exports: [
    MergeExtendedPipe,
    SlotToTokensPipe,
    SlotToTopicsPipe,
    ReplaceRiskPipe,
    StripPrefixPipe,
    RoundPredictionPipe
  ]
})
export class SharedModule {

}
