import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BreadcrumbModule} from 'xng-breadcrumb';

import {GenericViewComponent} from './generic-view.component';
import {GenericViewRoutingModule} from './generic-view-routing.module';
import {SharedComponentsModule} from '../shared-components/shared-components.module';
import {ViewsModule} from '../views/views.module';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [GenericViewComponent],
  imports: [
    CommonModule,
    GenericViewRoutingModule,
    SharedComponentsModule,
    BreadcrumbModule,
    ViewsModule,
    SharedModule
  ]
})
export class GenericViewModule {
}
