import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {GenericViewComponent} from './generic-view.component';
import {DiagnoseComponent} from '../views/diagnose/diagnose.component';


const routes: Routes = [{
  path: '',
  component: GenericViewComponent,
  children: [
    {
      path: '',
      redirectTo: 'diagnose',
      component: DiagnoseComponent
    },
    {
      path: 'diagnose',
      component: DiagnoseComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenericViewRoutingModule {
}
