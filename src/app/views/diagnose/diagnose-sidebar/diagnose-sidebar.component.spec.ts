import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DiagnoseSidebarComponent} from './diagnose-sidebar.component';
import {HttpClientModule} from '@angular/common/http';

describe('DiagnoseSidebarComponent', () => {
  let component: DiagnoseSidebarComponent;
  let fixture: ComponentFixture<DiagnoseSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [DiagnoseSidebarComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnoseSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
