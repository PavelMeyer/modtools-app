import {Component, OnDestroy, OnInit} from '@angular/core';
import {DiagnoseService} from '../../../shared/services/diagnose.service';
import {Subscription} from 'rxjs';
import {Topics} from '../../../../constants';
import {filter, tap} from 'rxjs/operators';

@Component({
  selector: 'diagnose-sidebar',
  templateUrl: './diagnose-sidebar.component.html',
  styleUrls: ['./diagnose-sidebar.component.less']
})
export class DiagnoseSidebarComponent implements OnInit, OnDestroy {

  public topicList = Topics;
  public policyChecks: { passesPrivateChat: boolean, passesGlobalChat: boolean };
  private subSync: Subscription[] = [];

  constructor(public diagnoseService: DiagnoseService) {
  }

  ngOnInit(): void {
    this.subSync.push(this.diagnoseService.classifyValue$.pipe(
      filter(classifyResult => !!classifyResult),
      tap(classifyResult => this.policyChecks = this.diagnoseService.getTopicGuidelines(classifyResult))
    ).subscribe())

  }

  ngOnDestroy(): void {
    this.subSync.forEach(sub => sub?.unsubscribe())
  }

}
