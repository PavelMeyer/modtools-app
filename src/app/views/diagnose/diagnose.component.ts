import {Component} from '@angular/core';
import {DiagnoseService} from '../../shared/services/diagnose.service';


@Component({
  selector: 'diagnose',
  templateUrl: './diagnose.component.html',
  styleUrls: ['./diagnose.component.less']
})
export class DiagnoseComponent {

  constructor(public diagnoseService: DiagnoseService) {
  }

}
