import {NgModule} from '@angular/core';
import {DiagnoseComponent} from './diagnose/diagnose.component';
import {CommonModule} from '@angular/common';
import {SharedComponentsModule} from '../shared-components/shared-components.module';
import {SharedModule} from '../shared/shared.module';
import {MainModule} from '../main/main.module';
import { DiagnoseSidebarComponent } from './diagnose/diagnose-sidebar/diagnose-sidebar.component';

@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    SharedModule,
    MainModule
  ],
  declarations: [DiagnoseComponent, DiagnoseSidebarComponent],
  exports: [DiagnoseComponent]
})
export class ViewsModule {

}
