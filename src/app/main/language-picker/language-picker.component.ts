import {Component, OnInit} from '@angular/core';
import {sortBy} from 'lodash';

import {Languages} from 'src/constants';
import {UserService} from 'src/app/shared-components/user.service';
import {LanguageService} from '../../shared/services/language.service';
import {LanguageCode} from '../../shared/models';

@Component({
  selector: 'language-picker',
  templateUrl: './language-picker.component.html',
  styleUrls: ['./language-picker.component.less']
})
export class LanguagePickerComponent implements OnInit {

  selectedLanguage: string;

  languages = [];

  constructor(
    private userService: UserService,
    private languageService: LanguageService
  ) {

  }

  async ngOnInit() {

    // Get the current user
    const user = await this.userService.me();

    // Filter the user's languages by the ones they're allowed to use
    if (user?.config?.allowedLanguages) {
      this.languages = Languages.filter(lang => user.config.allowedLanguages.includes(lang.code));
    }

    // Sort by language name, not code.
    this.languages = sortBy(this.languages, 'name');

    this.languageService.languageCode$.subscribe(code => {
      this.selectedLanguage = this.languages.find(lang => lang?.code === code) || this.selectedLanguage;
    });
  }

  /**
   * Fired when the user changes their language
   */
  onLanguageChanged(event: Event) {
    // todo fix typing for select change event
    // @ts-ignore
    this.languageService.setLanguageCode(event.currentTarget.value as LanguageCode)

  }

}
