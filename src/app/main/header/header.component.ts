import {Component, OnInit} from '@angular/core';

import {UserService} from 'src/app/shared-components/user.service';
import {User} from 'src/app/shared-components/user';

@Component({
  selector: 'main-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  user: User;

  constructor(public userService: UserService) {
  }

  async ngOnInit() {
    this.user = await this.userService.me();
  }

  onUserChangedClient(newClient: number) {
    // TODO #codereview This smells bad.
    // Oughtn't I be able to write this as this.UserService.preferences.language = newLanguage?
    const prefs = this.userService.preferences;
    prefs.lastClientId = newClient;
    this.userService.preferences = prefs;
  }

}
