import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl} from '@angular/forms';
import {ApiService} from '../../shared/services/api.service';
import {Subscription} from 'rxjs';
import {debounce, debounceTime, filter} from 'rxjs/operators';
import {DiagnoseService} from '../../shared/services/diagnose.service';

@Component({
  selector: 'main-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent implements OnInit, OnDestroy {

  searchControl: FormControl;
  private subSync: Subscription[] = [];

  constructor(private fb: FormBuilder, private diagnoseService: DiagnoseService) {
  }

  ngOnInit() {
    this.searchControl = this.fb.control([]);
    // this.subSync.push(
    //   this.searchControl.valueChanges
    //   .pipe(
    //     debounceTime(300),
    //     filter((value: string) => value && value.length >= 3)
    //   )
    //   .subscribe(value => {
    //     this.defaultService.classifyText(this.defaultService.plainTextToTextInput(value))
    //     .subscribe({
    //       next: (value => {
    //         debugger;
    //       }),
    //       error: err => {
    //         debugger;
    //       }
    //     })
    //   })
    // )
  }

  onSubmit() {
    this.diagnoseService.diagnoseText(this.searchControl.value)
    .subscribe()
  }

  ngOnDestroy(): void {
    this.subSync.forEach(sub => {
      sub?.unsubscribe()
    })
  }

}
